This repo just contains functions that I use in multiple projects to save 
rewritting code for checking if `malloc` returns a `NULL` pointer and things
like that.

TODO in the future:

- Add safe_scanf (makes sure correct number of arguments are read)
- Add safe_fopen
- Possibly convert to macros (stop function call overhead)
    - Could allows control over `assert` macro (Don't always exit program)