/* * * * * * *
 * Author: Nicholas Spain
 * Module of *safer* utilities for memory allocation
 * * * * * * */

#include <stdlib.h>
#include <stdio.h>

void *safe_malloc(size_t size) {
    void *ptr;
    ptr = malloc(size);
    if (!ptr) {
        fprintf(stderr, "Error in memory allocation");
        exit(EXIT_FAILURE);
    }
    return ptr;
}

void *safe_realloc(void *ptr, size_t size) {
    ptr = realloc(ptr, size);
    if (!ptr) {
        fprintf(stderr, "Error in memory allocation");
        exit(EXIT_FAILURE);
    }
    return ptr;
}

