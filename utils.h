
/* * * * * * *
 * Author: Nicholas Spain
 *
 * * * * * * */

#ifndef UTILS_H

#define UTILS_H

void *safe_malloc(size_t size);
void *safe_realloc(void *ptr, size_t size);

#endif
